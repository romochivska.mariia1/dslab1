#сайт Ільницького спортивно-гуманітарного ліцею
from requests import get
from bs4 import BeautifulSoup

url = "http://ilnlicey.ucoz.ua/"

FILE_NAME = "archive.txt"
with open(FILE_NAME, "w", encoding="utf-8") as file:
    page = get(url)
    soup = BeautifulSoup(page.content,  "html.parser")
    month_list = soup.find(class_="archUl")
    month_links = []

    for li in month_list.find_all("li"):
        a = li.find("a")
        month_name = a.find(text=True, recursive=False)
        month_link = url + a.get("href")
        month_links.append(month_link)

        print(f"Місяць з архіву: {month_name}")
        print(f"Посилання: {month_link}")
        file.write(f"Місяць з архіву: {month_name}\n")
        file.write(f"Посилання: {month_link}\n")

        month_page = get(month_link)
        soup = BeautifulSoup(month_page.content, "html.parser")
        news_list = soup.find(class_="content")

        if news_list:
            for li in news_list.find_all("li"):
                news_name = li.a.find(string=True, recursive=False)
                news_link = url + li.a.get("href")
                print(f"    Новини: {news_name}")
                print(f"    Посилання: {news_link}")
                file.write(f"Новини : {news_name}\n")
                file.write(f" Посилання: {news_link}\n")
